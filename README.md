# React App Test

![version](https://img.shields.io/badge/version-1.0.0-blue.svg)
![build](https://img.shields.io/badge/build-passing-green.svg)
![tests](https://img.shields.io/badge/tests-in%20review-red.svg)

react redux ui boilerplate, friends, lesson, friendship examples

## Getting Started

### Previous requirements
* npm
* craco
* [Git client](https://git-scm.com/)
* [API](https://gitlab.com/mdklv-boilerplate-stack/api-restful-node)


#### Important dependency
This project needs the project API running.

Mount and lauch the project according the instructions in [https://gitlab.com/mdklv-boilerplate-stack/api-restful-node](https://gitlab.com/mdklv-boilerplate-stack/api-restful-node)


### Clone or manage your code in Fork of the original reposiory
You can use simply doing a clone of the repo and using into a local environment
```
git clone https://gitlab.com/mdklv-boilerplate-stack/react-typescript-ui.git
```
But, this is not your place to made push request, so, you must make a Fork of the project, you can follow an step by step instruction [here](https://blog.scottlowe.org/2015/01/27/using-fork-branch-git-workflow/)

Follow the steps:
```
* Making a local clone
$ git clone https://gitlab.com/mdklv-boilerplate-stack/api-restful-node.git
* Create a [Branch](https://12factor.net/codebase)
$ git checkout -b <<id-gitlab-ticket>>-my-feature
```


## Setup
### 1. Config environment
#### 1.1. The .env file
Copy .env.example to .env as a first step, then use setup your config in .env
```
cp .env.example .env
```

The project is set to run with a minimal config (marked as optional in the example below)
Setup the address of the API. This is the unique mandatory dependency


#### 1.2 Environment values reference
```
# FIREBASE CONFIG
REACT_APP_FIREBASE_API_KEY=               (optional)
REACT_APP_FIREBASE_AUTH_DOMAIN=           (optional)
REACT_APP_FIREBASE_PROJECT_ID=            (optional)
REACT_APP_FIREBASE_STORAGE_BUCKET=        (optional)
REACT_APP_FIREBASE_MESSAGING_SENDER_ID=   (optional)
REACT_APP_FIREBASE_APP_ID=                (optional)
REACT_APP_FIREBASE_MEASUREMENT_ID=        (optional)
# SENTRY
REACT_APP_SENTRY_DSN=                     (optional)
# FULLSTORY
REACT_APP_FULLSTORY_ORG_ID=               (optional)
# MIXPANEL
REACT_APP_MIXPANEL_TOKEN=                 (optional)
# API
REACT_API_HOST=http://localhost:3001      (mandatory, URL and port of API)
```



### 2. Install modules node_modules and launch
Over the folder ./ run npm i
```
$ npm i
$ npm start
```

### 3. Test
In your browser test the address, where port is the environment variable seeted
```
open in your browser http://localhost:<PORT>/
```
