const path = require('path');
module.exports = {
    webpack: {
        alias: {
          "@components": path.resolve(__dirname, 'src/app/components/'),
          "@api": path.resolve(__dirname, 'src/app/api/'),
          "@assets": path.resolve(__dirname, 'src/app/assets/'),
          "@constants": path.resolve(__dirname, 'src/app/constants/'),
          "@services": path.resolve(__dirname, 'src/app/services/'),
          "@types": path.resolve(__dirname, 'src/app/types/'),
          "@redux": path.resolve(__dirname, 'src/app/redux/'),
          "@app": path.resolve(__dirname, 'src/app/'),
          "@base": path.resolve(__dirname, 'src/'),

        }
      },
    style: {
        postcss: {
            plugins: [
                require('tailwindcss'),
                require('autoprefixer'),
            ],
        },
    }
}
