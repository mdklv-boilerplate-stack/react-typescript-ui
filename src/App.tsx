import React from 'react';
import './App.css';
import {Seo} from "@components/seo/Seo";
import {Routes} from "@app/routes/Routes";
import {Menu} from "@components/common/Menu";

function App() {
  return (
      <>
        <Seo/>
        <Routes/>
      </>
  );
}
export default App;
