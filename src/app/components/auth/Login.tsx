import React, {useEffect} from 'react';
import { useDispatch } from 'react-redux';
import logo from '@assets/img/logo.png'

import {GoogleAuthProvider, FacebookAuthProvider} from "firebase/auth";

import { useSelector } from '@app/store';
import { login } from '@redux/slices/auth.slice';
import { isUserAuthenticatedSelector } from '@redux/selectors/auth.selector';
import { AuthState } from '@app/types/index';

export const Login = (props:any) => {
    const authenticated = useSelector(isUserAuthenticatedSelector);
    const dispatch = useDispatch();
    const appLogin = (provider: any) => {
        const userData:AuthState = {
            displayName: null,
            email: null,
            providerId: provider
        };
        dispatch(login(userData));
    };

    useEffect(() => {
        if (authenticated) {
            props.history.push('/home');
        }
    },[authenticated])

    return (
        <div>
            <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
                <div className="max-w-md w-full space-y-8">
                    <div>
                        <img
                            className="mx-auto h-24 w-auto"
                            src={logo}
                            alt="Logo MOP"
                        />
                        <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">Bienvenido!</h2>
                        <p className="mt-2 text-center text-sm text-gray-600">
                            <p className="font-medium text-indigo-600 hover:text-indigo-500">
                                Ingresa y comprueba el estado de tu siniestro
                            </p>
                        </p>
                    </div>
                    <div className=" mt-8 space-y-6 items-center content-center">
                        <button className=" group relative w-full  items-center justify-center py-2 px-4 border border-blue-500 text-md font-medium rounded-md  bg-transparent hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500" onClick={() => {appLogin(new FacebookAuthProvider())}}>
                                <span className="absolute left-0 inset-y-0 flex items-center pl-3">

                                </span>
                            Ingresa con Facebook
                        </button>
                        <button className="group relative w-full justify-center py-2 px-4 border border-red-400 text-md font-medium rounded-md  bg-transparent hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500" onClick={() => {appLogin(new GoogleAuthProvider())}}>
                                 <span className="absolute left-0 inset-y-0 flex items-center pl-3">

                                </span>
                            Ingresa con Google
                        </button>
                        <button className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-yellow-500 hover:bg-yellow-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-yellow-500" onClick={() => {appLogin(null)}}>Ingresar sin autenticarme</button>
                    </div>
                </div>
            </div>
        </div>
    );
};
