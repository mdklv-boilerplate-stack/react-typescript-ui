import React from 'react';
import ArrowLeft from "@mui/icons-material/ArrowLeft";
import ArrowRight from "@mui/icons-material/ArrowRight";

export interface IPropsBottom {
  currentPageSize: number,
  onChangePageSize: any,
  onPrev: any,
  onNext: any,
}
export const ListBottom = (props:IPropsBottom) => {
  return (
      <div className="py-3 text-right flex justify-between">
        <div className="flex justify-start">
          <select className="" onChange={(e)=>props.onChangePageSize(parseInt(e.target.value))} >
              <option selected={10==props.currentPageSize} value={10}>10</option>
              <option selected={20==props.currentPageSize} value={20}>20</option>
              <option selected={50==props.currentPageSize} value={50}>50</option>
              <option selected={100==props.currentPageSize} value={100}>100</option>
          </select>
          <label className="mt-3 ml-3 h-auto">Elementos por pagina</label>
        </div>
        <div className="flex">

              <div className="justify-end">
                    <button
                        type="button"
                        onClick={(e)=>{props.onPrev(e)}}
                        className="ml-2 mr-2 pl-2 pr-2 pt-2 pb-2 bg-gray-200 hover:bg-gray-400 cursor-pointer" >
                        <ArrowLeft/>
                        Anterior
                    </button>
                    <button
                        type="button"
                        onClick={(e)=>{props.onNext(e)}}
                        className="ml-2 mr-2 pl-2 pr-2 pt-2 pb-2 bg-gray-200 hover:bg-gray-400 cursor-pointer" >
                        Siguiente
                        <ArrowRight fontSize='small'/>
                    </button>
              </div>
        </div>
      </div>
    )
}
