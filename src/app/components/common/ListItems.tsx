import React from 'react';
import ArrowLeft from "@mui/icons-material/ArrowLeft";
import ArrowRight from "@mui/icons-material/ArrowRight";
import { LoadingTable } from "@components/common/LoadingTable";

export interface IPropsItems {
  itemsTitle:Array<string>
  items:any,
  loading: boolean
}
const titles = (itemsTitle:Array<string>) => {
  return itemsTitle.map( (x:string) => {
      return (
        <th
            scope="col"
            className="py-2 px-2 text-left text-xs font-medium text-gray-500"
        >
            {x}
        </th>
      )
  });
}

export const ListItems = (props:IPropsItems) => {
  return (
    <table className="min-w-full max-w-screen divide-y divide-gray-200 table-auto">
            <thead className="bg-gray-50 border ">
            <tr className="text-sm">
              { titles(props.itemsTitle) }
            </tr>
            </thead>
            <tbody className=" divide-y divide-gray-200">
              {
                props.loading?<LoadingTable/>:
                props.items
              }
            </tbody>
    </table>
  )
}
