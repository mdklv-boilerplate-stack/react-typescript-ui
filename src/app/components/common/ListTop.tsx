import React from 'react';
import Search from "@mui/icons-material/Search";
import Refresh from "@mui/icons-material/Refresh";

export interface IPropsTop {
  onChangeText: any,
  currentText: string,
  onClick: any,
  onRefresh: any
}
export const ListTop = (props:IPropsTop) => {
  return (
    <div>
          <div className="flex pt-4 pb-4 w-1/2">
            <input type="text" className="w-1/2 p-2" onChange={props.onChangeText} placeholder="buscar" value={props.currentText}/>
            <button
                onClick={(e)=>{props.onClick(e)}}

                type="button"
                className="ml-2 mr-2 pl-2 pr-2 bg-gray-200 hover:bg-gray-400 cursor-pointer"
            ><Search/>Buscar
            </button>
            <button
                type="button"
                onClick={(e)=>{props.onRefresh(e)}}
                  className="ml-2 mr-2 pl-2 pr-2 bg-gray-200 hover:bg-gray-400 cursor-pointer"
            >
            <Refresh/>
            </button>
          </div>

    </div>

  )
}
