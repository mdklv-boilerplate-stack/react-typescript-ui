export const LoadingTable = () => {
    return (
        <div className="rounded-md  lg:w-5/6 ">
            <div className="animate-pulse flex space-x-4">
                <div className="flex-1 space-y-4 py-2">
                    <div className="space-y-2 py-8">
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                      <div className="h-8 bg-gray-300 rounded w-full"></div>
                    </div>
                </div>
            </div>
        </div>
    );
}
