import React,{ useState} from 'react'
import {Link, useLocation} from 'react-router-dom';

import Close from "@mui/icons-material/Close";
import PinDrop from "@mui/icons-material/PinDrop";
import MenuIcon from "@mui/icons-material/Menu";

import logo from "@assets/img/logo.png";


export const Menu = (props:any) => {
    const [menuCollapsed, setMenuCollapsed] = useState(false);
    const [isCollapsible, setIsCollapsible ] = useState(false);

    const navigation = [
        { name: 'Structure', href: '/project/structure',lock: false},
        { name: 'Users', href: '/user/list',lock: false},
        { name: 'Lessons', href: '/lesson/list',lock: false},
        { name: 'Friendship', href: '/friendship/list',lock: true},
    ];
    const handleClick = (e:any) => {
      if (isCollapsible) setMenuCollapsed(true)
    }

    const items = () => {
      if (!menuCollapsed){
        return (
          navigation.map( (item,idx) => { return (

              <Link onClick={handleClick}
                  key={idx}
                  to={item.href}>
                  <li className="hover:bg-zinc-400 w-56 min-w-full  p-2 m-2 border-b hover:bg-blue-200">
                    {item.name}
                  </li>
              </Link>

          )})
        )
      } else return null;
    }
    return (

          <div className="min-h-screen min-w-64 border-gray border-r p-5 z-100">
              <div className="">
                <div className="">
                {
                  menuCollapsed?
                    <a onClick={  (e)=>{ setMenuCollapsed(false) } } className="cursor-pointer" >
                        <MenuIcon/>
                    </a>
                :
                    <div className="flex flex-wrap relative w-56">
                      <div className="right-0 absolute ">
                          <a onClick={ (e) => { setMenuCollapsed(true) }} className="cursor-pointer" >
                              <Close/>
                          </a>
                          <a onClick={ (e) => { setIsCollapsible(!isCollapsible) }} className="cursor-pointer" >
                              <PinDrop className={isCollapsible?"text-gray-200":""}/>
                          </a>
                      </div>
                    </div>
                }
                </div>
                <div>
                  <a>
                      {
                        !menuCollapsed &&
                          <img className={menuCollapsed?"w-10":"w-12 p-1"} src={menuCollapsed?logo:logo} alt="Test"/>
                      }
                  </a>
                </div>
                {
                  !menuCollapsed &&
                  <div className="full pb-2 pt-2">
                    <h3>Menu</h3>
                  </div>
                }

                <div></div>
                <ul>
                 {items()}
                </ul>
              </div>
          </div>
    );
}
