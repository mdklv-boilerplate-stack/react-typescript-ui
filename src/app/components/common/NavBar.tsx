import React from 'react'
import {useDispatch} from "react-redux";
import {useTranslation} from "react-i18next";
import logo from "@assets/img/logo.png";

import {getItem} from "@app/utils/storage";
import {logout} from "@redux/slices/auth.slice";
import {useAuth} from "@app/services/firebase/firebase";

export const NavBar = () => {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const uAuth = useAuth();
    const user = uAuth?uAuth.currentUser:{isAnonymous:true,displayName:"guest"};
    const signOut = () => {
        dispatch(logout())
    }
    return (
        <div className="absolute right top">
            <div className="md:flex items-center justify-end md:flex-1">
                <div className={"mb-4 mr-8"}>
                    {user?.isAnonymous ? <></>:<p className="font-semibold">{user?.displayName}</p>}
                </div>
                <div >

                </div>
            </div>
        </div>
    )
}
