import React from 'react';
import {Link, useLocation} from 'react-router-dom';

export const Title = (props:any) => {
    return (
        <h1 className="min-w-full text-4xl border-b border-gray-800 font-bold">{props.text}</h1>
    );
}
