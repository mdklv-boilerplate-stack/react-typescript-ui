export const Warning = () => {
    return (
        <div className="bg-yellow-600 mt-6">
            <div className="max-w-7xl ml-auto py-4 ">
                <p className="font-medium text-white text-center truncate">
                    <span className="sm:w-full xs:w-full md:inline font-bold">RECUERDA CAMBIAR LA CONFIGURACION DE LOS SERVICIOS EXTERNOS</span>
                </p>
            </div>
        </div>
    );
}
