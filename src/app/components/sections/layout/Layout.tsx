import React, {FC, useEffect, useRef, useState} from "react";
import {useSelector} from "react-redux";

import {NavBar} from "@components/common/NavBar";
import {Menu} from "@components/common/Menu";

export const Layout = ({children}:any) => {
    return(
        <div className="flex " >
            <div className="basis-1/12" >
                <Menu/>
            </div>
            <div className="basis-11/12 grow w-full bg-blue-400 p-10">
              {children}
            </div>
        </div>

    );
}
