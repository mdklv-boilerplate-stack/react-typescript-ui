import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import {Link, useLocation} from 'react-router-dom';

import { Title } from "@components/common/Title";
import { Lessons } from "@components/sections/user/Lessons";
import { Friends } from "@components/sections/user/Friends";
import { itemSelector, loadSelector } from "@app/redux/selectors/user.selector";
import { getUser } from "@app/redux/slices/user.slice";
import { User } from "@app/types/models/user";


export const Detail = (props:any) =>  {
    const dispatch = useDispatch();
    const loading = useSelector(loadSelector);
    const item = useSelector(itemSelector);

    useEffect(() => {
        dispatch(getUser( props?.match.params.id ));
    },[]);
    return (
          <div>
            <Title text="User Detail" ></Title>
            <form className="p-4">
              <fieldset >
                <legend>User ID </legend>
                <label className="font-bold">{item?.id}</label>
              </fieldset>
              <fieldset>
                <legend>User Name </legend>
                <label className="font-bold">{item?.name}</label>
              </fieldset>
              <fieldset>
                <legend>Lessons </legend>
                <Lessons id={ props?.match.params.id }/>
              </fieldset>
              <fieldset>
                <legend>Friends </legend>
                <Friends id={ props?.match.params.id }/>
              </fieldset>
            </form>
          </div>

    );
}
