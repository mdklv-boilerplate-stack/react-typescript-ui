import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";

import { listSelector,loadSelector } from "@app/redux/selectors/user.friends.selector";
import { getUserFriends } from "@app/redux/slices/user.slice";

import { User as UserModel } from "@app/types/models/user";

export interface IFriends {
  id: any
}
export const Friends = (props:IFriends) =>  {
    const dispatch = useDispatch();
    const loading = useSelector(loadSelector);
    const userfriends = useSelector(listSelector);

    useEffect(() => {
       dispatch(getUserFriends( props.id ));
    },[]);

    return (
        <ul>
        {
          userfriends?.map( (l:UserModel) => {
                return (
                    <li className="font-bold min-w-full">
                          <span>{l.name}</span>
                    </li>
                )
          })
        }
        </ul>
    );
}
