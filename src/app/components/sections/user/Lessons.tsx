import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";

import { listSelector,loadSelector } from "@app/redux/selectors/user.lesson.selector";
import { getUserLesson } from "@app/redux/slices/user.slice";

import { Lesson as LessonModel } from "@app/types/models/lesson";

export interface ILessons {
  id: any
}
export const Lessons = (props:ILessons) =>  {
    const dispatch = useDispatch();
    const loading = useSelector(loadSelector);
    const userlessons = useSelector(listSelector);

    useEffect(() => {
       dispatch(getUserLesson( props.id ));
    },[]);

    return (
        <ul>
        {
          userlessons?.map( (l:LessonModel) => {
                return (
                    <li className="font-bold min-w-full">
                          {l.name}
                    </li>
                )
          })
        }
        </ul>
    );
}
