import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import {Link, useLocation} from 'react-router-dom';

import { Title } from "@components/common/Title";
import { ListTop } from "@components/common/ListTop";
import { ListBottom } from "@components/common/ListBottom";
import { ListItems } from "@components/common/ListItems";

import { listSelector,loadSelector } from "@app/redux/selectors/users.selector";
import { getUsers } from "@app/redux/slices/user.slice";

import { User } from "@app/types/models/user";

export const List = () =>  {
    const dispatch = useDispatch();
    const [currentPage, setCurrentPage] = useState<number>(0);
    const [currentText, setCurrentText] = useState<string>('')
    const [currentPageSize, setCurrentPageSize] = useState<number>(10)

    const loading = useSelector(loadSelector);
    const items = useSelector(listSelector);
    useEffect(() => {
        updateList( currentPage, currentPageSize, currentText );
    },[]);
    const updateList = ( page:number, size:number, text:string ) => {
      dispatch(getUsers( { page: page, pagesize: size, text: text }));
    }
    const handleText = (e:any) => {
        setCurrentText(e.target.value);
        updateList(currentPage,currentPageSize,e.target.value);
    }
    const handleNext = (e:any) => {
        let p = currentPage + 1;
        setCurrentPage( p );
        updateList(p,currentPageSize,currentText);
    }
    const handlePrev = (e:any) => {
        let p = currentPage - 1;
        p = p<0?0:p;
        setCurrentPage( p );
        updateList(p,currentPageSize,currentText);
    }
    const handleElements = (value:number) => {
        setCurrentPageSize( value );
        updateList(currentPage,value,currentText);
    }

    const handleRefresh = (e:any) => {
        updateList(currentPage,currentPageSize,currentText);
    }
    const handleClick = (e:any) => {
        updateList(currentPage,currentPageSize,currentText);
    }
    const element = (user: User, key: string) => {
        switch (key){
            case "id":
              return user.id;
              break;
            case "name":
              return user.name;
              break;
          }
    }

    const Items = (keys:string[],items?: User[]|null|undefined) => {
      return items?.map( (u:User) => { return (
        <tr
            className="py-2 px-2 text-left text-sm font-medium text-gray-500 max-w-lg"
        >
          {
            keys.map((k:string) => { return (
              <td className="px-4 py-2 grow max-w-lg bg-white">
                <Link to={`/user/${u.id}/detail`}>
                    {element(u,k)}
                </Link>
              </td>
            )})
          }
        </tr>
      )
      });
    };



    return (
      <div>
        <Title text="User List" ></Title>
        <div>
          <ListTop onChangeText={handleText} currentText={currentText} onClick={handleClick} onRefresh={handleRefresh}/>
          <ListItems itemsTitle={["id","name"]} items={Items(["id","name"],items)} loading={loading||false}/>
          <ListBottom currentPageSize={currentPageSize} onChangePageSize={handleElements} onPrev={handlePrev} onNext={handleNext}/>
        </div>
      </div>
    );
}
