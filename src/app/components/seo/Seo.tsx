import React from "react";
import Helmet from "react-helmet";
import config from "../../../content/meta/config";

export const Seo = () => {
  return (
    <Helmet
      htmlAttributes={{
        lang: config.siteLanguage,
        prefix: "og: http://ogp.me/ns#"
      }}
    >
      {/* General tags */}
      <title>{config.siteTitle}</title>
      <meta name="description" content={config.siteDescription} />
      <link rel="canonical" href={config.siteUrl} />
      {/* OpenGraph tags */}
      <meta property="og:url" content={config.siteUrl} />
      <meta property="og:title" content={config.siteTitle} />
      <meta property="og:description" content={config.siteDescription} />
      <meta property="og:image" content={config.siteImage} />
      <meta property="og:keywords" content={config.siteKeywords} />
      <meta property="og:type" content="website" />
      {/*<meta property="fb:app_id" content={facebook.appId} />*/}
      {/* Twitter Card tags */}
      <meta name="twitter:card" content="summary" />
      <meta
        name="twitter:creator"
        content={config.twitter.creator}
      />
      <meta property="twitter:creator:id" content={config.twitter.creatorId} />
      <meta property="twitter:site" content={config.twitter.site} />
      <meta property="twitter:site:id" content={config.twitter.siteId} />
    </Helmet>
  );
};
