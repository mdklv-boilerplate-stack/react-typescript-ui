const dotenv = require('dotenv');
dotenv.config();
export const SENTRY_DSN = process.env.REACT_APP_SENTRY_DSN;
export const FULLSTORY_ORG_ID = process.env.REACT_APP_FULLSTORY_ORG_ID;
export const MIXPANEL_TOKEN = process.env.REACT_APP_MIXPANEL_TOKEN;
