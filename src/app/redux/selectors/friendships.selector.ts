import { createSelector } from 'reselect';
import { RootState } from '@app/store';
import { FriendshipsState } from "@app/types/interfaces/index";

export const friendshipSelector: (state: RootState) => FriendshipsState = (
    state: RootState
) => state.friendships;


export const listSelector = createSelector(friendshipSelector, combiner => {
    return combiner.friendships;
});

export const loadSelector = createSelector(friendshipSelector, combiner => {
    return combiner.loading;
});

export const errorSelector = createSelector(friendshipSelector, combiner => {
    return combiner.error;
});

export const friendshipStateSelector = createSelector(friendshipSelector, combiner => {
    return combiner;
});
