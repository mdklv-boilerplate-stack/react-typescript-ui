import { createSelector } from 'reselect';
import { RootState } from '@app/store';
import { LessonsState } from "@app/types/interfaces/index";

export const lessonsSelector: (state: RootState) => LessonsState = (
    state: RootState
) => state.lessons;


export const listSelector = createSelector(lessonsSelector, combiner => {
    return combiner.lessons;
});

export const loadSelector = createSelector(lessonsSelector, combiner => {
    return combiner.loading;
});

export const errorSelector = createSelector(lessonsSelector, combiner => {
    return combiner.error;
});

export const lessonStateSelector = createSelector(lessonsSelector, combiner => {
    return combiner;
});
