import { createSelector } from 'reselect';
import { RootState } from '@app/store';
import { UserFriendsState } from "@app/types/interfaces/index";

export const userFriendsSelector: (state: RootState) => UserFriendsState = (
    state: RootState
) => state.userFriends;


export const listSelector = createSelector(userFriendsSelector, combiner => {
    return combiner.users;
});
export const loadSelector = createSelector(userFriendsSelector, combiner => {
    return combiner.loading;
});

export const errorSelector = createSelector(userFriendsSelector, combiner => {
    return combiner.error;
});
