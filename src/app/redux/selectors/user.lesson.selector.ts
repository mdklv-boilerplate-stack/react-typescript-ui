import { createSelector } from 'reselect';
import { RootState } from '@app/store';
import { UserLessonState } from "@app/types/interfaces/index";

export const userLessonSelector: (state: RootState) => UserLessonState = (
    state: RootState
) => state.userLesson;


export const listSelector = createSelector(userLessonSelector, combiner => {
    return combiner.lessons
});
export const loadSelector = createSelector(userLessonSelector, combiner => {
    return combiner.loading;
});

export const errorSelector = createSelector(userLessonSelector, combiner => {
    return combiner.error;
});
