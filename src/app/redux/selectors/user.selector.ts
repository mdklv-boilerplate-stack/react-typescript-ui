import { createSelector } from 'reselect';
import { RootState } from '@app/store';
import { UserState } from "@app/types/interfaces/index";

export const userSelector: (state: RootState) => UserState = (
    state: RootState
) => state.user;


export const itemSelector = createSelector(userSelector, combiner => {
    return combiner.user
});

export const loadSelector = createSelector(userSelector, combiner => {
    return combiner.loading;
});

export const errorSelector = createSelector(userSelector, combiner => {
    return combiner.error;
});

export const userStateSelector = createSelector(userSelector, combiner => {
    return combiner;
});
