import { createSelector } from 'reselect';
import { RootState } from '@app/store';
import { UsersState } from "@app/types/interfaces/index";

export const usersSelector: (state: RootState) => UsersState = (
    state: RootState
) => state.users;


export const listSelector = createSelector(usersSelector, combiner => {
    return combiner.users;
});

export const loadSelector = createSelector(usersSelector, combiner => {
    return combiner.loading;
});

export const errorSelector = createSelector(usersSelector, combiner => {
    return combiner.error;
});

export const userStateSelector = createSelector(usersSelector, combiner => {
    return combiner;
});
