import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {
    browserSessionPersistence,
    getAuth,
    User,
    UserCredential,
    IdTokenResult,
    setPersistence,
    signInWithEmailAndPassword,
    signInWithPopup,
    signOut,
    AuthProvider,
    GoogleAuthProvider,
    FacebookAuthProvider
} from "firebase/auth";

import { AuthState, PayLoad} from "@app/types/index";
import {captureException} from "@services/tracking/sentry";

const initialState: AuthState = {
    displayName: undefined,
    email: undefined,
    authenticated: undefined,
    error: undefined,
    password: undefined,
    providerId: null,
    token: undefined,
    policies: []
};

export const extractClaims = async( credential: User ) => {
  let token:IdTokenResult = await credential.getIdTokenResult(false);
  let claims = token.claims as any;
  return claims
}

export const login = createAsyncThunk<AuthState, AuthState>(
    'login',
    async (req, thunkAPI) => {
        const auth = getAuth();
        const {email, password} = req;
        if (req.providerId == 'google.com' || req.providerId == 'facebook.com'){
          try {
            let prov:AuthProvider|undefined= undefined;;
            switch (req.providerId) {
              case "google.com":
                prov = new GoogleAuthProvider();
                break;
              case "facebook.com":
                prov = new FacebookAuthProvider();
                break;

            }
            if (prov) {
                let credential: UserCredential = await signInWithPopup(auth,prov);
                let token:IdTokenResult = await credential.user.getIdTokenResult(false);
                let claims = token.claims as any;
                console.log(claims);
                let agents = [...claims?.agents] ?? [];
                let agent:string|undefined = agents.shift();
                await setPersistence(auth,browserSessionPersistence);
                let pay:AuthState =
                  {
                    providerId: req.providerId,
                    displayName: credential.user.displayName,
                    email: credential.user.email ,
                    token: token,
                    policies: claims.policies
                }
                return pay;
            } else {
              return thunkAPI.rejectWithValue({ error: "fail" });
            }
          } catch (e) {
              captureException(e);
            return thunkAPI.rejectWithValue({ error: "fail" });
          }
        }
        if (req.providerId ===  "firebase" && password) {
          try {
            let credential: UserCredential = await signInWithEmailAndPassword(auth, email ? email : '' , password ? password:'');
            let token:IdTokenResult = await credential.user.getIdTokenResult(false);
            let claims = token.claims as any;
            let agents = claims?.agents || [];
            let agent:string|undefined = agents.shift();
            await setPersistence(auth,browserSessionPersistence)
            let pay:AuthState =
              {
                providerId: req.providerId,
                displayName: credential.user.displayName,
                email: credential.user.email ,
                token: token,
                policies: claims.policies
            }
            return pay;
          } catch (e) {
            captureException(e);
            return thunkAPI.rejectWithValue({ error: "fail" });
          }
        }
        if (req.token && req.token.expirationTime > new Date().toUTCString() ) {
          try {
            let token = req.token;
            let claims = token.claims as any;
            let agents = claims?.agents || [];
            const {displayName,email} = req;
            await setPersistence(auth,browserSessionPersistence)
            let pay:AuthState =
              {
                providerId: req.providerId,
                displayName: displayName,
                email: email,
                token: token,
                policies: claims.policies
            }
            return pay;
          } catch (e) {
              captureException(e);
            return thunkAPI.rejectWithValue({ error: "fail" });
          }
        }

        return thunkAPI.rejectWithValue({ error: "fail" });

    }
);

export const logout = createAsyncThunk('logout', async (_, thunkAPI) => {
    try {
        await signOut(getAuth());
    } catch (error:any) {
        console.log(error)
        return thunkAPI.rejectWithValue({ error: error.message });
    }
});

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(login.fulfilled, (state, action) => {
            state.displayName = action.payload.displayName;
            state.email = action.payload.email;
            state.authenticated = true;
            state.providerId = action.payload.providerId;
            state.token = action.payload.token;
            state.policies = action.payload.policies;
        });
        builder.addCase(login.rejected, (state, action) => {
            state.error = action.error;
        });
        builder.addCase(logout.fulfilled, state => {
            state.authenticated = false;
            state.displayName = initialState.displayName;
            state.email = initialState.email;
            state.providerId = initialState.providerId;
            state.token = initialState.token;
            state.policies = initialState.policies;
        });
        builder.addCase(logout.rejected, (state, action) => {
            state.error = action.error;
        });
    },
});
