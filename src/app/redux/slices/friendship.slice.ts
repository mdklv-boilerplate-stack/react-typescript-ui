import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit";
import {captureException} from "@services/tracking/sentry";
import {GeneralResponse} from "@app/types/interfaces";

import {FriendshipsState} from "@app/types/interfaces/index";
import {FriendshipApi} from "@app/services/api/friendship-api";

const initialState: FriendshipsState = {
    friendships: undefined,
    loading: false,
    error: undefined,
    response: undefined,
};

export const getFriendship = createAsyncThunk(
    'friendship/list',
    async (params:any, thunkAPI) => {
        try {
            return await new FriendshipApi().friendshipGet( params.page, params.pagesize, params.text )
                .then((response: any) => {
                    return {friendships:response.data} as FriendshipsState
                })
                .catch((error) => {
                    captureException(error);
                });

        } catch (error:any) {
            captureException(error);
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
);

export const friendshipsSlice = createSlice({
  name: 'friendship/list/get',
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) =>{
    builder.addCase( getFriendship.pending, (state,action) => {
      state.loading = true;
      state.friendships = undefined;
    });
    builder.addCase( getFriendship.rejected, (state,action) => {
      state.loading = false;
      state.error = action.error;
      state.friendships = undefined;
    });
    builder.addCase( getFriendship.fulfilled, (state,action: PayloadAction<any>) => {
      state.loading = false;
      state.error = undefined;
      state.friendships = action?.payload?.friendships;
    })
  }
})
