import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit";
import {captureException} from "@services/tracking/sentry";
import {GeneralResponse} from "@app/types/interfaces";

import {LessonsState} from "@app/types/interfaces/index";
import {LessonApi} from "@app/services/api/lesson-api";

const initialState: LessonsState = {
    lessons: undefined,
    loading: false,
    error: undefined,
    response: undefined,
};

export const getLessons = createAsyncThunk(
    'lesson/list',
    async (params:any, thunkAPI) => {
        try {
            return await new LessonApi().lessonGet( params.page, params.pagesize, params.text )
                .then((response: any) => {
                    return {lessons:response.data} as LessonsState
                })
                .catch((error) => {
                    captureException(error);
                });

        } catch (error:any) {
            captureException(error);
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
);

export const lessonsSlice = createSlice({
  name: 'lesson/list/get',
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) =>{
    builder.addCase( getLessons.pending, (state,action) => {
      state.loading = true;
      state.lessons = undefined;
    });
    builder.addCase( getLessons.rejected, (state,action) => {
      state.loading = false;
      state.error = action.error;
      state.lessons = undefined;
    });
    builder.addCase( getLessons.fulfilled, (state,action: PayloadAction<any>) => {
      state.loading = false;
      state.error = undefined;
      state.lessons = action?.payload?.lessons;
    })
  }
})
