import {createAsyncThunk, createSlice, PayloadAction} from "@reduxjs/toolkit";
import {captureException} from "@services/tracking/sentry";

import {UserState,UsersState,UserLessonState,UserFriendsState} from "@app/types/interfaces/index";
import {UserApi} from "@app/services/api/user-api";


const initialStateUsersState: UsersState = {
    users: undefined,
    loading: false,
    error: undefined,
    response: undefined,
};

const initialStateUserState: UserState = {
    user: undefined,
    loading: false,
    error: undefined,
    response: undefined,
};

const initialStateUserLesson: UserLessonState = {
    lessons: undefined,
    loading: false,
    error: undefined,
    response: undefined,
};

const initialStateUserFriends: UserFriendsState = {
    users: undefined,
    loading: false,
    error: undefined,
    response: undefined,
};


export const getUsers = createAsyncThunk(
    'user/list',
    async (params:any, thunkAPI) => {
        try {
            return await new UserApi().userGet( params.page, params.pagesize, params.text )
                .then((response: any) => {
                    return {users:response.data} as UsersState
                })
                .catch((error) => {
                    captureException(error);
                });

        } catch (error:any) {
            captureException(error);
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
);

export const getUser = createAsyncThunk(
    'user/getById',
    async (id:any, thunkAPI) => {
        try {
            return await new UserApi().userIdGet(id)
                .then((response: any) => {
                    return {user:response.data} as UserState
                })
                .catch((error) => {
                    captureException(error);
                });

        } catch (error:any) {
            captureException(error);
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
);

export const getUserLesson = createAsyncThunk(
    'user/lesson/list',
    async (id:any, thunkAPI) => {
        try {
            return await new UserApi().userIdLessonGet( id )
                .then((response: any) => {
                    return {lessons:response.data} as UserLessonState
                })
                .catch((error) => {
                    captureException(error);
                });
        } catch (error:any) {
            captureException(error);
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
);


export const getUserFriends = createAsyncThunk(
    'user/lesson/list',
    async (id:any, thunkAPI) => {
        try {
            return await new UserApi().userIdFriendGet( id )
                .then((response: any) => {
                    return {users:response.data} as UserFriendsState
                })
                .catch((error) => {
                    captureException(error);
                });
        } catch (error:any) {
            captureException(error);
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
);



export const usersSlice = createSlice({
  name: 'users/slices',
  initialState: initialStateUsersState,
  reducers: {},
  extraReducers: (builder) =>{
    builder.addCase( getUsers.pending, (state,action) => {
      state.loading = true;
      state.users = undefined;
    });
    builder.addCase( getUsers.rejected, (state,action) => {
      state.loading = false;
      state.error = action.error;
      state.users = undefined;
    });
    builder.addCase( getUsers.fulfilled, (state,action: PayloadAction<any>) => {
      state.loading = false;
      state.error = undefined;
      state.users = action?.payload?.users;
    });
  }
})

export const userSlice = createSlice({
  name: 'user/slices',
  initialState: initialStateUserState,
  reducers: {},
  extraReducers: (builder) =>{
    builder.addCase( getUser.pending, (state,action) => {
      state.loading = true;
      state.user = undefined;
    });
    builder.addCase( getUser.rejected, (state,action) => {
      state.loading = false;
      state.error = action.error;
      state.user = undefined;
    });
    builder.addCase( getUser.fulfilled, (state,action: PayloadAction<any>) => {
      state.loading = false;
      state.error = undefined;
      state.user = action?.payload?.user;
    })
  }
})



export const userLessonSlice = createSlice({
  name: 'user/lessons/slices',
  initialState: initialStateUserLesson,
  reducers: {},
  extraReducers: (builder) =>{
    builder.addCase( getUserLesson.pending, (state,action) => {
      state.loading = true;
      state.lessons = undefined;
    });
    builder.addCase( getUserLesson.rejected, (state,action) => {
      state.loading = false;
      state.error = action.error;
      state.lessons = undefined;
    });
    builder.addCase( getUserLesson.fulfilled, (state,action: PayloadAction<any>) => {
      state.loading = false;
      state.error = undefined;
      state.lessons = action?.payload?.lessons;
    });
  }
})


export const userFriendsSlice = createSlice({
  name: 'user/lessons/slices',
  initialState: initialStateUserFriends,
  reducers: {},
  extraReducers: (builder) =>{
    builder.addCase( getUserLesson.pending, (state,action) => {
      state.loading = true;
      state.users = undefined;
    });
    builder.addCase( getUserLesson.rejected, (state,action) => {
      state.loading = false;
      state.error = action.error;
      state.users = undefined;
    });
    builder.addCase( getUserLesson.fulfilled, (state,action: PayloadAction<any>) => {
      state.loading = false;
      state.error = undefined;
      state.users = action?.payload?.users;
    });
  }
})
