import React, { useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { getAuth, onAuthStateChanged } from 'firebase/auth';

import { useSelector } from "@app/store";
import { login, logout } from "@redux/slices/auth.slice";
import { isUserAuthenticatedSelector , authSelector} from "@redux/selectors/auth.selector";
import { useAuth } from "@app/services/firebase/firebase";
import {PrivateRoute} from "./PrivateRoute";

import {Login} from "@components/auth/Login";
import {Home} from "@components/sections/home/Home";
import {Layout} from "@components/sections/layout/Layout";
import {Landing} from "@components/sections/home/Landing";

import { List as UserList} from "@components/sections/user/List";
import { List as LessonList} from "@components/sections/lesson/List";
import { List as FriendshipList} from "@components/sections/friendship/List";

import { Detail as UserDetail} from "@components/sections/user/Detail";


export const Routes = () => {
    const authenticated = useSelector(isUserAuthenticatedSelector);
    const dispatch = useDispatch();
    const auth = useAuth();

    const refresh = useCallback(
        async (displayName, email, providerId, password, token, agent) => {
            const userData = {
                displayName,
                email,
                providerId,
                password,
                token,
                agent

            };

            //TODO DISPATH LOGIN
            //return dispatch(login(userData));
        },
        [dispatch]
    );

    useEffect(() => {
        if (auth) {
          const f = async () => {
                onAuthStateChanged(auth, async (user) => {
                    if (user && !authenticated) {
                        let password = undefined;
                        let agent = localStorage.getItem("selected_agent");
                        return await refresh( user.displayName, user.email, user.providerId, password, await user.getIdTokenResult(false), agent );
                    }
                    if (!user && !authenticated) {
                        dispatch(logout());
                    }
                });
          };
          f();
        }
    }, [authenticated]);


    return (
          <Router>
              <Switch>
                  <Layout>
                    <Route exact path='/' component={Landing} />
                    <PrivateRoute exact path='/user/list' component={UserList} isAuthenticated={true}/>
                    <PrivateRoute exact path='/lesson/list' component={LessonList} isAuthenticated={true}/>
                    <PrivateRoute exact path='/friendship/list' component={FriendshipList} isAuthenticated={true}/>
                    <PrivateRoute exact path='/user/:id/detail' component={UserDetail} isAuthenticated={true}/>
                    <PrivateRoute exact path='/project/structure' component={Landing} isAuthenticated={true} />

                  <PrivateRoute
                          path='/home'
                          isAuthenticated={authenticated}
                          component={Home}
                    />
                  </Layout>
              </Switch>
          </Router>
    );

}
