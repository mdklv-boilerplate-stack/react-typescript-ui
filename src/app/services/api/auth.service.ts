import { getAuth, signInWithPopup, signOut } from "firebase/auth";
const auth = getAuth();
export const socialLogin = async (provider: any) =>{
    return await signInWithPopup(auth, provider)
        /*.then(result => {
            const response = result
            console.log(result);
            return result;
        })
        .catch(error => {
            console.error(error)
            return rejects(error, error.message);
        });*/
}

export const logout = async () => {
    await signOut(auth);
}
