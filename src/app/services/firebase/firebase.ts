import config from "./config";
import {initializeApp} from "firebase/app";
import {
    getAuth
} from "firebase/auth";
import { getRemoteConfig, getAll, fetchAndActivate} from "firebase/remote-config";
import { getDatabase, ref, set, get, child} from "firebase/database";

import {captureException} from "@app/services/tracking/sentry";
import {putItem} from "@app/utils/storage";
import {User} from "@app/types/index";

let app:any|undefined=null;
if (config && config.apiKey)
  app = initializeApp(config);

if (config && config.apiKey && app) {
  const remoteConfig = getRemoteConfig(app);
  remoteConfig.settings.minimumFetchIntervalMillis = 3600;
  remoteConfig.defaultConfig = {
      "title": "Base Template"
  };
  fetchAndActivate(remoteConfig)
  .then(() => {
      const allKeys = getAll(remoteConfig);
      if(allKeys !== undefined && allKeys !== null){
          const keys = Object.entries(allKeys);
          keys.forEach((obj) => {
              putItem(obj[0], obj[1].asString());
          });
      }
  })
  .catch((err) => {
      captureException(err);
  });
}

/**
 * Get auth from firebase context, even anon
 */
export const useAuth = () => {
    return app?getAuth():null;

}

/**
 * Save user session on Real Time Database
 * @param user
 * @type User
 */
export const saveUserDate = (user: User) => {
    const db = getDatabase();
    set(ref(db, 'users/' + user.uid), {
        email: user.email,
        profile_picture : user.profilePicture,
        display_name: user.displayName,
        signed_in: user.signedIn
    });
}

/**
 * Get user by uid from Real Time Database
 * @param userId
 * @return User
 */
export const getUserData = (userId:string) => {
    const dbRef = ref(getDatabase());
    get(child(dbRef, `users/${userId}`)).then((snapshot) => {
        if (snapshot.exists()) {
            return snapshot.val();
        } else {
            console.log("No data available");
        }
    }).catch((error) => {
        captureException(error)
    });
}
