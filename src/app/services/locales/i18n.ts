import i18n from 'i18next';
import {initReactI18next} from "react-i18next";
import es from "./es/translation.json";
import en from "./en/translation.json";
const availableLanguages = ['es', 'en'];
export const myresources = {
    es: {
        messages: es
    },
    en:{
        messages: en
    }
} as const;

i18n.use(initReactI18next).init({
    lng: 'es',
    debug: true,
    supportedLngs: availableLanguages,
    load: 'all',
    ns:['messages'],
    fallbackLng: "es",
    resources: myresources
});
