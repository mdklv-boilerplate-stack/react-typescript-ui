import * as FullStory from '@fullstory/browser';
import {FULLSTORY_ORG_ID} from "@app/constants/common.constants";
import {captureException} from "@app/services/tracking/sentry";
import {User} from "@app/types/index";

export const init = () => {
    try {
        const orgId = FULLSTORY_ORG_ID ? FULLSTORY_ORG_ID : '';
        FullStory.init({ orgId: orgId });
    }catch (e){
        captureException(e);
    }
}

export const identify = (user:User) => {
    if (typeof user?.uid === "string") {
        FullStory.identify(user?.uid, {
            displayName: user?.displayName?.toString(),
            email: user?.email?.toString(),
        });
    }
}
