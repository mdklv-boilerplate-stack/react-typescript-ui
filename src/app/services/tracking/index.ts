import {captureException, init as initSentry, identify as identifySentry} from './sentry'
import {init as initFullStory, identify as identifyFS} from "./fullstory";
import {init as initMixPanel, identify as identifyMP} from "./mixPanel";
import {User} from "@app/types/index";

export const boot = () => {
   initSentry();
   initFullStory();
   initMixPanel();
}

export const identify = (user:User) => {
   try{
      identifySentry(user);
      identifyFS(user);
      identifyMP(user);
   }catch (e){
      captureException(e);
   }
}
