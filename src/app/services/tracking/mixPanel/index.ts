import {MIXPANEL_TOKEN} from "@app/constants/common.constants";
import {captureException} from "@app/services/tracking/sentry";
import {User} from "@app/types/index";
const mixpanel = require('mixpanel-browser');

export const init = () => {
    try {
        mixpanel.init(MIXPANEL_TOKEN);
    }catch (e){
        captureException(e);
    }
}

export const identify = (user:User) => {
    try {
        mixpanel.identify(user.uid);
        mixpanel.alias(user.email, user.email);
        mixpanel.people.set({
            $name: user.displayName,
            $created: (new Date()).toISOString(),
            $email: user.email
        });
    }catch (e) {
        captureException(e);
    }
}
