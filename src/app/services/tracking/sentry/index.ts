import * as Sentry from "@sentry/react";
import {Integrations} from "@sentry/tracing";
import {SENTRY_DSN} from "@app/constants/common.constants";
import {User} from "@app/types/index";

export const init = () => {
    try{
        Sentry.init({
            dsn: SENTRY_DSN,
            integrations: [new Integrations.BrowserTracing()],
            tracesSampleRate: 1.0,
        });
    }catch (e){
        console.error(e);
    }
}

export const captureException = (ex: any) => {
    Sentry.captureException(ex);
}

export const identify = (user: User) => {
    Sentry.setUser({ id: user?.uid?.toString(), email: user?.email?.toString() });
}
