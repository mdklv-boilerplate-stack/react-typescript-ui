import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import {authSlice} from "./redux/slices/auth.slice";
import {userSlice,usersSlice,userLessonSlice,userFriendsSlice} from "./redux/slices/user.slice";
import {lessonsSlice} from "./redux/slices/lesson.slice";
import {friendshipsSlice} from "./redux/slices/friendship.slice";

import {
  useSelector as rawUseSelector,
  TypedUseSelectorHook,
} from 'react-redux';
export const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
    users: usersSlice.reducer,
    user: userSlice.reducer,
    userLesson: userLessonSlice.reducer,
    userFriends: userFriendsSlice.reducer,
    lessons: lessonsSlice.reducer,
    friendships: friendshipsSlice.reducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
export const useSelector: TypedUseSelectorHook<RootState> = rawUseSelector;
