import {SerializedError} from "@reduxjs/toolkit";
import {IdTokenResult} from "firebase/auth";

export interface AuthState {
    displayName?: string | null;
    email?: string | null;
    password?: string | null;
    authenticated?: boolean;
    error?: SerializedError;
    providerId: string | null;
    token?: IdTokenResult | undefined;
    policies?: Array<{action:string,object:string}>[] | null,
}

export interface PayLoad {
    displayName?: string | null;
    email?: string | null;
    provider: any;
}

export interface User {
    uid?: string | null ,
    email?: string | null,
    profilePicture?: string | null,
    displayName?: string | null,
    signedIn?: boolean;
}
