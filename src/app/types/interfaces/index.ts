import {SerializedError} from "@reduxjs/toolkit";
import { User } from "../models/user";
import { Lesson } from "../models/lesson";
import { Friendship } from "../models/friendship";

export interface GeneralResponse {
    message: string;
    payload: string;
}
export interface IStateStatus {
  loading: boolean | null,
  error?: SerializedError;
  response?: GeneralResponse | null
}
export interface UsersState extends IStateStatus {
    users?: Array<User> | null,
}

export interface UserState extends IStateStatus{
    user?: User | null,
}

export interface UserLessonState extends IStateStatus {
    lessons?: Array<Lesson> | null,
}

export interface UserFriendsState extends IStateStatus {
    users?: Array<User> | null,
}
export interface LessonsState extends IStateStatus {
    lessons?: Array<Lesson> | null,
}
export interface FriendshipsState extends IStateStatus {
    friendships?: Array<Friendship> | null,
}
