/**
 * Put item to local storage <strong>(NOT USE WITH SENSITIVE DATA)</strong>
 * @param key
 * @param value
 */

export const putItem = (key:string, value:any) => {
    localStorage.setItem(key, value);
}

/**
 * Get item from local storage
 * @param key
 */
export const getItem = (key:string) => {
    return localStorage.getItem(key);
}
