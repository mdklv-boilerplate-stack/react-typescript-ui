module.exports = {
  siteTitle: "Boilerplate UI MDK",
  shortSiteTitle: "MDKB",
  siteDescription: "Boilerplate UI MDK",
  siteUrl: "localhost:3000",
  siteImage: "logo.png",
  siteLanguage: "es",
  siteKeywords: "",
  /* author */
  authorName: "Pablo",

  /* manifest.json */
  manifestName: "Pablo",
  manifestShortName: "Pablo", // max 12 characters
  manifestStartUrl: "/index.html",
  manifestBackgroundColor: "white",
  manifestThemeColor: "#666",
  manifestDisplay: "standalone",

  // Use your Gravatar image.
  // Replace your email adress with md5-code.
  // Example https://www.gravatar.com/avatar/nicolas@momentofpeople.co ->
  // gravatarImgMd5: "https://www.gravatar.com/avatar/3c70c791b8b8960d94c1559eed236f2c",
  gravatarImgMd5: "https://www.gravatar.com/avatar/3c70c791b8b8960d94c1559eed236f2c",

  // social
  authorSocialLinks: [
    { name: "twitter", url: "https://twitter.com" },
    { name: "facebook", url: "http://facebook.com" }
  ],
  twitter :{
    creator: "",
    creatorId: "",
    site: "",
    siteId: ""
  }
};
