import React from 'react';
import { hydrate, render } from "react-dom";
import './index.css';
import App from './App';
import { store } from './app/store';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import {boot} from "./app/services/tracking";
import './app/services/locales/i18n';
import './app/services/firebase/firebase'

boot();
const rootElement = document.getElementById("root");
if (rootElement?.hasChildNodes()) {
    hydrate(
        <React.StrictMode>
            <Provider store={store}>
                <App />
            </Provider>
        </React.StrictMode>
        , rootElement);
} else {
    render(
        <React.Fragment>
            <Provider store={store}>
                <App />
            </Provider>
        </React.Fragment>
        , rootElement);
}

serviceWorker.unregister();
